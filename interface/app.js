const {ipcRenderer} = require("electron");
const ipc = ipcRenderer;


//Boutton CLOSE REDUCE

const closeBtn = document.getElementById("closeBtn");
const reduceBtn = document.getElementById("reduceBtn");
const settingsBtn = document.getElementById("settingsBtn");
const networkBtn = document.getElementById("networkBtn")
const logsBtn = document.getElementById("logsBtn")


console.log("[Karl Client] : Check : LINKJS_COMPLETE_APP_JS : Done");

closeBtn.addEventListener("click", () => {
    console.log("[Karl Client] : Script Balise : IPC_MESSAGE_CLOSE: Send");
    ipc.send("close");
    
});

reduceBtn.addEventListener("click", () => {

    console.log("[Karl Client] : Script Balise : IPC_MESSAGE_REDUCE: Send");
    ipc.send("reduce");
}); 

settingsBtn.addEventListener("click", () => {
    ipc.send("showSettings")

})

networkBtn.addEventListener("click", () => {

    ipc.send("showNetwork")

})

logsBtn.addEventListener("click", () => {

    ipc.send("showLogs")

})


