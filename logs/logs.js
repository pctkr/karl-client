const { time } = require("console");
const {ipcRenderer} = require("electron");
const ipc = ipcRenderer;
const fs = require("fs")


//Boutton CLOSE REDUCE

const closeBtn = document.getElementById("closeBtn");
const reduceBtn = document.getElementById("reduceBtn");
const backButton = document.getElementById("backBtn");
const logInterface = document.getElementById("dialogLog")
const closeLogBtn = document.getElementById("backLogBtn")
const reloadBtn = document.getElementById("actBtn")
const conteneur = document.getElementById("conteneur")


console.log("[Karl Client] : Check : LINKJS_COMPLETE_APP_JS : Done");

closeBtn.addEventListener("click", () => {
    console.log("[Karl Client] : Script Balise : IPC_MESSAGE_CLOSE: Send");
    ipc.send("close");
    
});

reduceBtn.addEventListener("click", () => {

    console.log("[Karl Client] : Script Balise : IPC_MESSAGE_REDUCE: Send");
    ipc.send("reduce");
}); 

backButton.addEventListener("click", () => {
    ipc.send("showMain")

})

reloadBtn.addEventListener("click", () => {
    reloadBtn.innerHTML = '<i style="color : grey;" class="fas fa-spin fa-sync"></i>'
    reloadBtn.disabled = true
    reloadBtn.classList.remove("actBtn")
    reloadBtn.classList.add("actBtnDisabled")
    setTimeout(() => {
        getLogsFolder()
        reloadBtn.disabled = false
        reloadBtn.classList.add("actBtn")
        reloadBtn.classList.remove("actBtnDisabled")
    }, 1000) 
    
   
})


closeLogBtn.addEventListener("click", () => {
    const contentVLog = document.getElementById("cLog")
    contentVLog.scrollTop = 0;
    logInterface.close()
})

getLogsFolder()

//  [Function] - Find Folder files and show them

function getLogsFolder() {


const date = new Date()
const logsFolder = ipc.sendSync("getLogsFolder")
const dateLastAct = document.getElementById("dateLastAct")

var gmonth = date.getMonth()
var gday = date.getDate()
var gHour = date.getHours()
var gMinute = date.getMinutes()
var gSecondes = date.getSeconds()


if(date.getMonth() + 1 <= 9) {
    gmonth = "0" + (date.getMonth() + 1)
}

if(date.getDate() + 1 <= 9) {
    gday = "0" + date.getDate()
}

if(date.getHours() + 1 <= 9) {
    gHour = "0" + date.getHours()
}

if(date.getMinutes() + 1 <= 9) {
    gMinute = "0" + date.getMinutes()
}

if(date.getSeconds() + 1 <= 9) {
    gSecondes = "0" + date.getSeconds()
}

var currentDate = gday + "/" + gmonth + "/" + date.getFullYear() + " " + gHour + "h"  + "-" + gMinute + "m" + "-" + gSecondes + "s" 


dateLastAct.innerHTML = currentDate

conteneur.innerHTML = '<table class="fileExplorerTable" id="fileExplorerTable"><tr><td>Pas de donnée</td><td><div></div></td></tr></table>'

var tableReg = document.getElementById("fileExplorerTable")


tableReg.deleteRow(0)

reloadBtn.innerHTML = '<i class="fas fa-sync"></i>'

if(logsFolder.length == 0) {

    var row = tableReg.insertRow(0)
    var cell1 = row.insertCell(0)
    var cell2 = row.insertCell(1)
    

    cell1.innerHTML ="Aucune log trouvé"
    cell2.innerHTML = "Verifier si le dossier de log existe !"

    

} else {

for(const file of logsFolder) {


    

        const nameWOExt = file.replace(".log", "")
        const nameDecompiled =  nameWOExt.split("-")
        const FileDate = nameDecompiled[2] + "/" + nameDecompiled[1] + "/" + nameDecompiled[0] + " - " + nameDecompiled[3] + nameDecompiled[4] + nameDecompiled[5]  
        
        var row = tableReg.insertRow(0)
        var cell1 = row.insertCell(0)
        var cell2 = row.insertCell(1)
        var cell3 = row.insertCell(2)

        
        

    cell1.innerHTML = file;
    if(nameDecompiled[0] == date.getFullYear()) {
        cell2.innerHTML = FileDate

    } else {

        cell2.innerHTML = "<span style='color:red;'>Pas de date</span>"
    }
   
    cell3.innerHTML = '<div style="display: flex; justify-content: space-around;"><button class="showBtn" id="' + file + ".showBtn" + '"><i class="fa fa-eye"></i></button><button class="deleteBtn" id="' + file + "deleteBtn" + '"><i class="fa fa-trash"></i></button></div>' ;
    
   

    const showBtn = document.getElementById(file + ".showBtn")
    showBtn.addEventListener("click", () => {

        
        const contentLog = document.getElementById("cLog")
        
        contentLog.innerHTML = '<table class="logContentTable" id="logContentTable"><tr><td>Aucune log trouvé</td><td>Pas de donnée</td></tr></table>'
        
        const logData = ipc.sendSync("getLogData", file)
        const nameLog = document.getElementById("nameOfFileLabel")
        nameLog.innerHTML = file
        const logLines = logData.split("\n")
        const logContentTable = document.getElementById("logContentTable")   
           
            logContentTable.deleteRow(0)
            

            var lineNumber = 0

            logLines.forEach((line) => {

                var lineDeconstruct = line.split("-")
                

                var row = logContentTable.insertRow(lineNumber)
                var cell1 = row.insertCell(0)
                var cell2 = row.insertCell(1)
                var cell3 = row.insertCell(2)
                var cell4 = row.insertCell(3)
                

                const dateV = new Date()

            

                if(lineDeconstruct[0] == "[" + dateV.getFullYear()) {

                    
                    
                    cell1.innerHTML = '<span style="color: orange;">' + (lineNumber + 1) + "</span>"
                    cell2.innerHTML = '<span style="color: green;">' + lineDeconstruct[0].replace("[", "") + "-" + lineDeconstruct[1] + "-" + lineDeconstruct[2] + "-"  + lineDeconstruct[3] + "-"  + lineDeconstruct[4] + "-" +  lineDeconstruct[5].replace("]", "") + '</span>'
                    
                    if(lineDeconstruct[6].replace("[", "").replace("]", "") == " INFO ") {
                        cell3.innerHTML = '<span style="color: blue;">' + lineDeconstruct[6].replace("[", "").replace("]", "") + '</span>'

                    } else if(lineDeconstruct[6].replace("[", "").replace("]", "") == " WARN ") {
                        cell3.innerHTML = '<span style="color: yellow  ;">' + lineDeconstruct[6].replace("[", "").replace("]", "") + '</span>'

                    } else if(lineDeconstruct[6].replace("[", "").replace("]", "") == " ERROR ") {
                        cell3.innerHTML = '<span style="color: red ;">' + lineDeconstruct[6].replace("[", "").replace("]", "") + '</span>'

                    } else if(lineDeconstruct[6].replace("[", "").replace("]", "") == " FATAL ") {
                        cell3.innerHTML = '<span style="color: darkred;">' + lineDeconstruct[6].replace("[", "").replace("]", "") + '</span>'

                    } else {
                        
                        cell3.innerHTML = '<span style="color: red;">Niveau non reconnu</span>'
                    }
                    
                   
                    cell4.innerHTML = line.replace(lineDeconstruct[0] + "-", "").replace(lineDeconstruct[1] + "-", "").replace(lineDeconstruct[2] + "-", "").replace(lineDeconstruct[3] + "-", "").replace(lineDeconstruct[4] + "-", "").replace(lineDeconstruct[5] + "-", "").replace(lineDeconstruct[6] + "-", "")
                   
                    

                } else {

                    cell1.innerHTML = '<span style="color: orange;">' + (lineNumber + 1) + "</span>"
                    cell2.innerHTML = "Pas de date"
                    cell3.innerHTML = "Aucun niveau"
                    cell4.innerHTML = logLines[lineNumber]
                }

               

                lineNumber = lineNumber + 1;
           
            })
                            
            
        
        
        logInterface.showModal()

    })

    const deleteBtn = document.getElementById(file + "deleteBtn")
    deleteBtn.addEventListener("click", () => {
        ipc.send("deleteLogFile", file)
        ipc.send("showLogs")


    })
  
        
}

}

}











