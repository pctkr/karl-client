const fsI = require("fs")


// [Function] - Check Update !

const updateDialog = document.getElementById("updateShowLog")
const backUpdBtn = document.getElementById("cancelUpdate")
const checkUpdateBtnE = document.getElementById("checkUpdateBtn")
const validInsBtn = document.getElementById("validUpdate")


function showDialogUpdate() {

    if(fsI.existsSync("latest-update.json")) {

        backUpdBtn.addEventListener("click", () => {
            fsI.rmSync("latest-update.json")
            updateDialog.close()
    
        })

        validInsBtn.addEventListener("click", () => {

            updateDialog.close()
            ipc.send("validInstall")

        })

            var updateData = JSON.parse(fsI.readFileSync("latest-update.json"))
            const versionUpdLabel = document.getElementById("versionUpdLabel")
            const dateUpdLabel = document.getElementById("dateLabel")
            const infoUpdLabel = document.getElementById("textUpdate")
    
                    
            versionUpdLabel.innerHTML = updateData.version
            dateUpdLabel.innerHTML = updateData.date
            infoUpdLabel.innerHTML = updateData.updateinfo
    
            updateDialog.showModal()

           
    
            ipc.send("update-show", "enable")
    
    } 
        
}

checkUpdateBtnE.addEventListener("click", () => {

        ipc.send("findUpdate")
        const infoUpd = document.getElementById("infoUpd")
        checkUpdateBtnE.classList.remove("checkUpdate")
        checkUpdateBtnE.classList.add("checkUpdateDis")
        checkUpdateBtnE.innerHTML = "<i style='color:white; widht: 50px; text-align: center;' class='fa fa-spin fa-gear'></i>"
        checkUpdateBtnE.disabled = true

        ipc.on("checkConnexion", (event, status) => {
            checkUpdateBtnE.innerHTML = "Chercher une mise à jour"
            checkUpdateBtnE.disabled = false
            checkUpdateBtnE.classList.add("checkUpdate")
            checkUpdateBtnE.classList.remove("checkUpdateDis")

            if(status == "1") {
                ipc.on("updateInfoDownload", () => {
    
                    
                    
                    if(!fs.existsSync(__dirname.replace("settings", "latest-update.json"))) {
                
                        infoUpd.innerHTML = "Cette version est à jour"
                
                    }
                    showDialogUpdate()
                })
                
            } else {
                infoUpd.innerHTML = "<span style='color: red;'>Aucune connexion internet !</span>"


            }
    
            
    

        })

       

})

ipc.send("checkAutoUpdate")

ipc.on("firstStartup", () => {
    var updateDialogCheck = ipc.sendSync("update-show", "check")

    
    if(updateDialogCheck == 0) {

            showDialogUpdate()
        

    } else if(updateDialogCheck == 1) {

          console.log("[Karl Client] - Already show !")
    } else {

       console.log("[Karl Client] - Nothing to show !")
    }

})

ipc.on("showUpdateDialog", () => {

    showDialogUpdate()
})

