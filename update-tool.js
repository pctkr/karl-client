//Interface du Centre de Contrôle personnel PCTKR - Karl Client
//Concu par Raphael PICOT - Raphix
//GITLAB : https://gitlab.com/pctkr/karl-client

// [Systeme] - Update Tool for Karl Client - Update : 1.0 - 07-2022

const nodePath = require('path');

const appWindow = require("./main.js");

const http = require('https'); 
const fs = require('fs');
const { app } = require('electron');



//[Settings] - Settings of Update

const UpdateUrl = "https://gitlab.com/pctkr/karl-client/-/raw/main/latest.json"
const updateNameFile = "latest-update.json"

//[Function] - Find Update - Search if an update is aviable

module.exports.findUpdate = (passFirstStartup) => {
    
    if(fs.existsSync(updateNameFile)) {

        fs.rmSync(updateNameFile)
    }
    
    const updateInfoFile = fs.createWriteStream(updateNameFile);
    const requestToGit = http.get(UpdateUrl, function(response) {
       
       response.pipe(updateInfoFile);
       console.log("[Karl Client] - Update Tool - Info -  Download Process - Update Info File : Processing ... ! From : " + UpdateUrl);
      
       updateInfoFile.on("finish", () => {
           updateInfoFile.close();
           console.log("[Karl Client] - Update Tool - Info -  Download Process - Update Info File : Complete ! From : " + UpdateUrl);

           const updateInfo = JSON.parse(fs.readFileSync(updateNameFile))
           const settingsFile = JSON.parse(fs.readFileSync("package.json"))

           function setFloatNumber(number) {
                var numberDecomposed = number.split(".")
                var goodFormat = numberDecomposed[0] + numberDecomposed[1]
                
                return goodFormat
           }

           if(parseFloat(setFloatNumber(updateInfo.version)) >= parseFloat(setFloatNumber(settingsFile.version))) {
                console.log("[Karl Client] - Update Tool - Info - New Update is aviable !")

                appWindow.get().webContents.send("updateInfoDownload")
                appWindow.get().webContents.send("firstStartup")

                if(passFirstStartup == true) {

                    appWindow.showUpdateDialog()
                }

           } else {
            console.log("[Karl Client] - Update Tool - Info - This version is up to date !");
            updateInfoFile.close();
            fs.rmSync(updateNameFile)
           }

           
       });
    }).on("error", (err) => {

       
       updateInfoFile.close()
       fs.rmSync(updateNameFile)
       console.log("[Karl Client] - Update Tool - Error - Download Update Info File Failed!");
       return "__DOWNLOAD_FAIL"
    });
    

}

//[Function] - Erase Data - Clean latest-update data

module.exports.eraseDate = () => {

    fs.rmSync(updateNameFile)
}

// [Function] - Download Update 

module.exports.downloadUpdate = () => {

    appWindow.get().close()
    appWindow.createUpdateLauncher()

}