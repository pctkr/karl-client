//Interface du Centre de Contrôle personnel PCTKR - Karl Client
//Concu par Raphael PICOT - Raphix
//GITLAB : https://gitlab.com/pctkr/karl-client


const {app, BrowserWindow, ipcMain, Notification, nativeImage, Tray, Menu} = require("electron")
const path = require("path")
const fs = require("fs")
const ipc = ipcMain
const updateTool = require("./update-tool.js")
const {async, await} = require("fs/promises")


// [Function] Main Window Function
function createWindow() {
    const win = new BrowserWindow({
        
        width: 900,
        height: 590,
        minWidth: 900,
        minHeight: 590,
        resizable: false,
        movable: true,
        closable: true,
        frame: false,
        icon: path.join(__dirname, './logo.ico'),
        title: "Karl",
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            devTools: true
        }

    })


   
    win.loadFile("interface/app.html")

    module.exports.get = function () {

        return win
    }

    module.exports.showUpdateDialog = function () {
        win.loadFile("settings/settings.html")
        new Notification({title: "Karl - Update", body: "Une mise à jour est disponible !"}).show()
        win.webContents.send("showUpdateDialog")
    }

    

    //[Function] - IPC Request

    ipc.on("close", () => {
        console.log("[Karl Client] : Script Balise : IPC_MESSAGE_CLOSE : Recu")
        console.log("[Karl Client] : Fermeture")
        if(fs.existsSync("latest-update.json")) {
            updateTool.eraseDate()

        }
       
        win.close()
    })

    ipc.on("reduce", () => {
        console.log("[Karl Client] : Script Balise : IPC_MESSAGE_REDUCE : Recu")
        console.log("[Karl Client] : Minimisation")
        win.minimize()
    })

    ipc.on("showSettings", () => {

        win.loadFile("settings/settings.html")

    })

    ipc.on("showMain", () => {

        win.loadFile("interface/app.html")

    })

    ipc.on("showNetwork", () => {

        win.loadFile("network/network.html")
    })

    ipc.on("showLogs", () => {

        win.loadFile("logs/logs.html")
    })


    ipc.on("findUpdate", () => {


        const httpP = require("https")
        const reqTest = httpP.get("https://gitlab.com", function(response) {
            win.webContents.send("checkConnexion", "1")
            updateTool.findUpdate()
           
           
        }).on("error", () => {

            win.webContents.send("checkConnexion", "0")
        })

    })

    var updateDialogShow = 0

    ipc.on("update-show", (event, operation) => {
        if(operation == "check") {
           
            event.returnValue = updateDialogShow
            
            
        }


        if(operation == "enable") {

            updateDialogShow = 1
        }

    })

    ipc.on("checkAutoUpdate", () => {

        if(updateDialogShow == 0) {

            var settingsData = JSON.parse(fs.readFileSync("settings.json"))

            if(settingsData.autoUpdate == "1") {
                
                updateTool.findUpdate()
                
        
            }

        }

        
    })

    ipc.on("validInstall", () => {
        updateTool.downloadUpdate()

    })

    ipc.on('sendUpdateNotif', () => {
        new Notification({title: "Karl", body: "Une nouvelle mise à jour est disponible ! Téléchargez là depuis l'application Karl !"}).show()
    })
  
    // [Function] - Show Logs Folder - Recieve from Karl
    const logsFolderDir = "C:/Users/picot/OneDrive/Bureau/Developement/Karl/karl/logs" 

    ipc.on("getLogsFolder", (event, arg) =>  {

        

        if(fs.existsSync(logsFolderDir)) {
            const logsFolder = fs.readdirSync("C:/Users/picot/OneDrive/Bureau/Developement/Karl/karl/logs")
        
            var FolderIndex = logsFolder.indexOf('history.txt');
            if (FolderIndex !== -1) {
                logsFolder.splice(FolderIndex, 1);
            }
            

            event.returnValue = logsFolder

        } else {

            const logsFolder = []
            event.returnValue = logsFolder
        }

        
        
    })

    // [Function] - Delete log in Log Folder - Send to Karl

    ipc.on("getLogData", (event, file) => {

        var lgdata = fs.readFileSync("C:/Users/picot/OneDrive/Bureau/Developement/Karl/karl/logs/" + file).toString()
        event.returnValue = lgdata
        
    })

    // [Function] - Delete Log File 

    ipc.on("deleteLogFile", (event, file) => {

        fs.rmSync(logsFolderDir + "/" + file)
        if(fs.existsSync(logsFolderDir + "/" + file)) {

            console.log("Error")
        }
    })


}

// [Function] - Update Window

module.exports.createUpdateLauncher = () => {

    const upd = new BrowserWindow({
        
        width: 500,
        height: 300,
        minWidth: 500,
        minHeight: 300,
        resizable: false,
        movable: true,
        closable: true,
        frame: false,
        icon: path.join(__dirname, './logo.ico'),
        title: "Karl - Updater",
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            devTools: true
        }

    })

    ipc.on("cancelUpdate", () => {
        app.quit()
        

    })

    ipc.on("endUpdate", () => {

        fs.cpSync("./newversion/", "./", {recursive: true})
        new Notification({title: "Karl - Update", body: "La mise à jour vient de se terminer !"}).show()
        fs.rmSync("./newversion", { recursive: true, force: true })
        app.relaunch()
        app.quit()
        
    })


    upd.loadFile("update-launcher/update.html")

}





// [Function] - Initialization of VIK


app.whenReady().then(() => {

    const icon = nativeImage.createFromPath('interface/logo.png')
    tray = new Tray(icon)


    const contextMenu = Menu.buildFromTemplate([
         { label: "Vérifier les mises à jours", click() { updateTool.findUpdate(true)}},
         { type: 'separator' },
         { label:"Quitter", click() { app.quit()}}
      ])
    
    tray.setToolTip("Karl")
    tray.setContextMenu(contextMenu)

    //[Operation] - Check settings

    if(!fs.existsSync("settings.json")) {

        var settingsFormat = {

            "autoUpdate":"1"
        }


        var settingsFormatT = JSON.stringify(settingsFormat, null, 2)

        fs.writeFileSync("settings.json", settingsFormatT)

        createWindow()

    } else {

        createWindow()
        
        app.on('activate', () => {

        if(BrowserWindow.getAllWindows().length === 0) {
                createWindow()   

        }

        }) 

    }

    

})

