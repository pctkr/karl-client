const {ipcRenderer} = require("electron");
const ipc = ipcRenderer;
const fs = require("fs")


//Boutton CLOSE REDUCE

const closeBtn = document.getElementById("closeBtn");
const reduceBtn = document.getElementById("reduceBtn");
const backButton = document.getElementById("backBtn");
const addFormBtn = document.getElementById("addNetworkBtn")
const closeFormBtn = document.getElementById("closeFormBtn")
const addNetForm = document.getElementById("addNetForm")
const addNetFormNext = document.getElementById("addNetFormNext")
const sendNetForm = document.getElementById("sendFormBtn")
const newAutoCoBox = document.getElementById("newAutoConnexionBox")
const closeFormNextBtn = document.getElementById("closeFormNextBtn")



console.log("[Karl Client] : Check : LINKJS_COMPLETE_APP_JS : Done");

closeBtn.addEventListener("click", () => {
    console.log("[Karl Client] : Script Balise : IPC_MESSAGE_CLOSE: Send");
    ipc.send("close");
    
});

reduceBtn.addEventListener("click", () => {

    console.log("[Karl Client] : Script Balise : IPC_MESSAGE_REDUCE: Send");
    ipc.send("reduce");
}); 

backButton.addEventListener("click", () => {
    ipc.send("showMain")

})


// [Function] - Send NewConnexionSessionForm

addFormBtn.addEventListener("click", () => {

    addNetForm.showModal()
    

})

closeFormBtn.addEventListener("click", () => {

    addNetForm.close()
    

})

closeFormNextBtn.addEventListener("click", () => {

    addNetFormNext.close()
})

sendNetForm.addEventListener("click", () => {

    addNetForm.close()
    addNetFormNext.showModal()
})

newAutoCoBox.addEventListener("change", () => {

    if(newAutoCoBox.checked == true) {
        newAutoCoBox.classList.add("fa-check", "fa", "tickUpd")

      
    } else if(newAutoCoBox.checked == false){

        
        newAutoCoBox.classList.remove("fa-check", "fa", "tickUpd")

    }
})




