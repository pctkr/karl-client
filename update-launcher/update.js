const { ipcRenderer } = require("electron")
const fs = require("fs")
const http = require("https")


const progressBar = document.getElementById("updateProcessBar")
const info = document.getElementById("updateInfoLabel")
const cancelButton = document.getElementById("stopBtn")

progressBar.value = 0
info.innerHTML = "Téléchargement en attente"

if(fs.existsSync("latest-update.json")) {

    fs.rmSync("latest-update.json")

}

if(fs.existsSync("newversion/")) {
            
    fs.rmSync("./newversion/", { recursive: true })
}


const requestFiles = http.get("https://gitlab.com/pctkr/karl-client/-/raw/main/files.txt", function (respI) {
    
    if(fs.existsSync("files.txt")) {
        fs.rmSync("files.txt")

    }
    const filesData = fs.createWriteStream("files.txt")
    respI.pipe(filesData)

    filesData.on("finish", () => {
        filesData.close()
        const fileR = JSON.parse(fs.readFileSync("files.txt"))
        const files = fileR

        var fileTotal = files.length
        var fileDownloaded = 0

        

        fs.rmSync("files.txt")

        
        
        files.forEach((file) => {
           
      
            console.log("[Karl Update] - Download file in process : " + file)

            
            const installFiles = http.get("https://gitlab.com/pctkr/karl-client/-/raw/main/" + file, function (respE) {
              
                var getDirName = require('path').dirname;

                function writeFileSync(path, contents) {
                    fs.mkdirSync(getDirName(path), { recursive: true})
                    fs.writeFileSync(path, contents);
                }
    
                writeFileSync("newversion/" + file, "")
               
                const fileDir = fs.createWriteStream("newversion/" + file, {
                    flags: 'a'
                
                })
               
               
                respE.pipe(fileDir)

                fileDir.on("finish", () => {
                    progressBar.value = fileDownloaded * 100 / fileTotal;
                    info.innerHTML = "<span style='color: white;'>Téléchagement en cours : " + file + " - " + (fileDownloaded * 100 / fileTotal) + "%" + "</span>"
    
                    if(fileDownloaded + 1 >= fileTotal) {
                        ipcRenderer.send("endUpdate")
                        console.log("[Karl Update] - Update finish")
                    }
                    fileDir.close()
                    console.log("[Karl Update] - Download file completed : " + file)
                    fileDownloaded++;

                })


            }).on("error", function() {

                cancelButton.classList.remove("notVisible")
                console.log("[Karl Update] - Error : File is not disponible : " + file)
                info.innerHTML = "<span style='color: red;'>Echec de la mise à jour - Fichier Indisponible</span>"

                if(fs.existsSync("latest-update.json")) {

                    fs.rmSync("latest-update.json")
                
                }
                
                if(fs.existsSync("newversion/")) {
                            
                    fs.rmSync("./newversion/", { recursive: true, force: true})
                }
                
                if(fs.existsSync("files.txt")) {
                    fs.rmSync("files.txt")
            
                }
            })


        })
        
       
    })


}).on("error", function () {
  
    cancelButton.classList.remove("notVisible")
    info.innerHTML = "<span style='color: red;'>Echec de la mise à jour</span>"

    if(fs.existsSync("latest-update.json")) {

        fs.rmSync("latest-update.json")
    
    }
    
    if(fs.existsSync("newversion/")) {
                
        fs.rmSync("newversion", { recursive: true, force: true })
    }
    
    if(fs.existsSync("files.txt")) {
        fs.rmSync("files.txt")

    }

})

cancelButton.addEventListener("click", () => {

   
    ipcRenderer.send("cancelUpdate")
    

})

