const {ipcRenderer} = require("electron");
const ipc = ipcRenderer;
const fs = require("fs")


//Boutton CLOSE REDUCE

const closeBtn = document.getElementById("closeBtn");
const reduceBtn = document.getElementById("reduceBtn");
const backButton= document.getElementById("backBtn");
const versionLabel = document.getElementById("version")
const searchUpdateBox = document.getElementById("searchUpdateBox")
const checkUpdateBtn = document.getElementById("checkUpdateBtn")


console.log("[Karl Client] : Check : LINKJS_COMPLETE_APP_JS : Done");

closeBtn.addEventListener("click", () => {
    console.log("[Karl Client] : Script Balise : IPC_MESSAGE_CLOSE: Send");
    ipc.send("close");
    
});

reduceBtn.addEventListener("click", () => {

    console.log("[Karl Client] : Script Balise : IPC_MESSAGE_REDUCE: Send");
    ipc.send("reduce");
}); 

backButton.addEventListener("click", () => {
    ipc.send("showMain")

})

searchUpdateBox.addEventListener("change", () => {

    if(searchUpdateBox.checked == true) {
        searchUpdateBox.classList.add("fa-check", "fa", "tickUpd")

        var oldset = JSON.parse(fs.readFileSync(__dirname.replace("settings", "settings.json")))

        Object.defineProperties(oldset, {

            autoUpdate: {
                value: "1",
                writable: true
            }
        })

        var newset = JSON.stringify(oldset, null, 2)

        fs.writeFileSync(__dirname.replace("settings", "settings.json"), newset)
        


    } else if(searchUpdateBox.checked == false){

        
        searchUpdateBox.classList.remove("fa-check", "fa", "tickUpd")

        var oldset = JSON.parse(fs.readFileSync(__dirname.replace("settings", "settings.json")))

        Object.defineProperties(oldset, {

            autoUpdate: {
                value: "0",
                writable: true
            }
        })

        var newset = JSON.stringify(oldset, null, 2)

        fs.writeFileSync(__dirname.replace("settings", "settings.json"), newset)
        
    }
})

checkUpdateBtn.addEventListener("click", () => {

  

})




// [Operation] - Find version of KarlClient



var vdata = fs.readFileSync(__dirname.replace("settings", "package.json"))

var ver = JSON.parse(vdata) 

versionLabel.innerHTML = "Version : " + ver.version + " - Nom du package : " + ver.name + "@" + ver.version

// [Operation] - Set If checked is always valid

var sdata = fs.readFileSync(__dirname.replace("settings", "settings.json"))

var settings = JSON.parse(sdata)

if(settings.autoUpdate == '1') {

    searchUpdateBox.classList.add("fa-check", "fa", "tickUpd")
    searchUpdateBox.checked = true
}